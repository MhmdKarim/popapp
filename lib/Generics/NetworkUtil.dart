import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:popapp/Models/api_response_model.dart';
import 'package:popapp/PreDefined/app_constants.dart';


class NetworkUtil {

  
// next three lines makes this class a Singleton
  //static NetworkUtil _instance = new NetworkUtil.internal();
  //NetworkUtil.internal();
  static String baseUrl = "https://lookin.sos-webapp.com/apiAgent/";
  //factory NetworkUtil() => _instance;

  static Future<ApiResponseModel> post(String url, String encodedBody,
      {Map<String, String> header}) async {
    final _encoding = Encoding.getByName('utf-8');

    final _headers = {
      'Content-Type': 'application/json',
      'lang': appLanguageCode,
      //'Authorization': constLoginModel.rememberToken
    };
    var response = await http.post(baseUrl + url,
        body: encodedBody, headers: _headers, encoding: _encoding);

    // The response body is an array of items
    if (response != null && response.statusCode == HttpStatus.ok) {
      return ApiResponseModel.fromJson(json.decode(response.body));
    } else {
      print('Post API Response Status code:' +
          response.statusCode.toString() +
          ' ' +
          response.body);
      return null;
    }
  }
 
  static Future<ApiResponseModel> postWithoutToken(
      String url, String encodedBody,
      {Map<String, String> header}) async {
    final _encoding = Encoding.getByName('utf-8');

    final _headers = {
      'Content-Type': 'application/json',
      'lang': appLanguageCode,
    };
    var response = await http.post(baseUrl + url,
        body: encodedBody, headers: _headers, encoding: _encoding);

    // The response body is an array of items
    if (response != null && response.statusCode == HttpStatus.ok) {
      return ApiResponseModel.fromJson(json.decode(response.body));
    } else {
      print('Post API Response Status code:' +
          response.statusCode.toString() +
          ' ' +
          response.body);
      return null;
    }
  }

 static Future<ApiResponseModel> postWithoutTokenFormData(
      String url, Map<String, dynamic> encodedBody,
      {Map<String, String> header}) async {
    final _encoding = Encoding.getByName('utf-8');

    final _headers = {
      //'Content-Type': 'application/form-data',
      'lang': appLanguageCode,
    };
    var response = await http.post(baseUrl + url,
        body: encodedBody, headers: _headers, encoding: _encoding);

    // The response body is an array of items
    if (response != null && response.statusCode == HttpStatus.ok) {
      return ApiResponseModel.fromJson(json.decode(response.body));
    } else {
      print('Post API Response Status code:' +
          response.statusCode.toString() +
          ' ' +
          response.body);
      return null;
    }
  }



  static Future<ApiResponseModel> postWithoutTokenAndcContent(
      String url, String encodedBody,
      {Map<String, String> header}) async {
    final _encoding = Encoding.getByName('utf-8');

    final _headers = {
      'lang': appLanguageCode,
    };
    var response = await http.post(baseUrl + url,
        body: encodedBody, headers: _headers, encoding: _encoding);

    // The response body is an array of items
    if (response != null && response.statusCode == HttpStatus.ok) {
      return ApiResponseModel.fromJson(json.decode(response.body));
    } else {
      print('Post API Response Status code:' +
          response.statusCode.toString() +
          ' ' +
          response.body);
      return null;
    }
  }

  static Future<ApiResponseModel> get(String url,
      {Map<String, String> header}) async {
    // final _encoding = Encoding.getByName('utf-8');

    final _headers = {
      'Content-Type': 'application/json',
      'lang': appLanguageCode,
      //'Authorization': constLoginModel.rememberToken
    };
    var response = await http.get(
      baseUrl + url,
      headers: _headers,
    );
   // print('rememberToken: '+constLoginModel.rememberToken);
    // The response body is an array of items
    if (response != null && response.statusCode == HttpStatus.ok) {
      return ApiResponseModel.fromJson(json.decode(response.body));
    } else {
      print('get API Response Status code:' +
          response.statusCode.toString() +
          ' ' +
          response.body);
      return null;
    }
  }


  static Future<ApiResponseModel> getWithoutToken(String url,
      {Map<String, String> header}) async {
    // final _encoding = Encoding.getByName('utf-8');

    final _headers = {
      'Content-Type': 'application/json',
      'lang': appLanguageCode
    };
    var response = await http.get(
      baseUrl + url,
      headers: _headers,
    );
    // The response body is an array of items
    if (response != null && response.statusCode == HttpStatus.ok) {
      return ApiResponseModel.fromJson(json.decode(response.body));
    } else {
      print('get API Response Status code:' +
          response.statusCode.toString() +
          ' ' +
          response.body);
      return null;
    }
  }

  static Future<ApiResponseModel> delete(String url,
      {Map<String, String> header}) async {
    // final _encoding = Encoding.getByName('utf-8');
    final _headers = {
      'Content-Type': 'application/json',
      'lang': appLanguageCode,
     // 'Authorization': constLoginModel.rememberToken
    };
    var response = await http.delete(baseUrl + url, headers: _headers);

    // The response body is an array of items
    if (response != null && response.statusCode == HttpStatus.ok) {
      return ApiResponseModel.fromJson(json.decode(response.body));
    } else {
      print('Post API Response Status code:' +
          response.statusCode.toString() +
          ' ' +
          response.body);
      return null;
    }
  }

  static Future<ApiResponseModel> put(String url,String encodedBody,
      {Map<String, String> header}) async {
    // final _encoding = Encoding.getByName('utf-8');
    final _headers = {
      'Content-Type': 'application/json',
      'lang': appLanguageCode,
     // 'Authorization': constLoginModel.rememberToken
    };
    var response = await http.put(baseUrl + url,body: encodedBody, headers: _headers);

    // The response body is an array of items
    if (response != null && response.statusCode == HttpStatus.ok) {
      return ApiResponseModel.fromJson(json.decode(response.body));
    } else {
      print('Post API Response Status code:' +
          response.statusCode.toString() +
          ' ' +
          response.body);
      return null;
    }
  }
}
