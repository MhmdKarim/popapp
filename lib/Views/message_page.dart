import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/Widgets/custom_appbar_widget.dart';
import 'package:popapp/Widgets/text_widgets.dart';

class MessagePage extends StatelessWidget {
static const String tag = '/PrivacyPolicyPage';

  @override
  Widget build(BuildContext context) {
    //SizeConfig().init(context);

    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: SafeArea(
         bottom: false,
              child: Scaffold(
                backgroundColor: primaryColor,
          appBar: CustomAppBar.appBar(title:"Message"),
          body: Container(
            child: Center(child:  MyText.lTextNL(text:"Message")),
          ),
        ),
      ),
    );
  }
}
