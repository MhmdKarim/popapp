import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/Widgets/custom_appbar_widget.dart';
import 'package:popapp/Widgets/photo_widget.dart';

class ProductsPage extends StatefulWidget {
  static const tag = '/ProductsPage';

  @override
  _ProductsPageState createState() => _ProductsPageState();
}

class _ProductsPageState extends State<ProductsPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      child: Scaffold(
        backgroundColor: primaryColor,
        appBar: CustomAppBar.appBar(title: "Products"),
        body: ListView(
          children: [
            Container(
              child: PhotoWidget(
                height: hBlock * 95,
                width: hBlock * 95,
              ),
            ),
            Container(
              height: hBlock * 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: 6,
                  itemBuilder: (context, index) {
                    return PhotoWidget(
                      height: hBlock * 35,
                      width: hBlock * 35,
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
