import 'dart:ui';
import 'dart:io' show Platform;

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/Views/booking_page.dart';
import 'package:popapp/Widgets/general_button_widget.dart';
import 'package:popapp/Widgets/red_curved_btn_widget.dart';
import 'package:popapp/Widgets/photo_widget.dart';
import 'package:popapp/Widgets/rating_widget.dart';
import 'package:popapp/Widgets/recent_reviews_widget.dart';
import 'package:popapp/Widgets/text_widgets.dart';
import 'package:social_media_buttons/social_media_button.dart';

class DetailsPage extends StatelessWidget {
  final _ratingController = TextEditingController();
  int _stars = 0;

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: SafeArea(
        bottom: false,
        child: Scaffold(
          backgroundColor: primaryColor,
          body: ListView(
            children: <Widget>[
              _topWidget(context),
              Container(height: 3, width: double.infinity, color: redColor),
              _title('About Bob'),
              // Container(
              //   height: hBlock * 40,
              //   child: ListView.builder(
              //       shrinkWrap: true,
              //       scrollDirection: Axis.horizontal,
              //       itemCount: 10,
              //       itemBuilder: (context, index) {
              //         return PhotoWidget(
              //           height: hBlock * 35,
              //           width: hBlock * 35,
              //           visible: false,
              //         );
              //       }),
              // ),
              _infoText(),
              _infoText(),
              _infoText(),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: MyText.sTextNL(
                    text:
                        'Robert Nesta Marley, OM (6 February 1945 – 11 May 1981) was a Jamaican singer, songwriter and musician. Considered one of the pioneers of reggae, his musical career was marked by fusing elements of reggae, ska, and rocksteady, as well as his distinctive vocal and songwriting style. Marley\'s contributions to '),
              ),
              _appsWidget(),
              _title('Musicianship'),
              _infoText(),
              _infoText(),
              _infoText(),
              _title('Prices'),
              _pricesList(),
              _title('Customer Questions'),
              _customerQuestions(),
              _addYourRating(context),
              _title('Recent Reviews'),
              RecentReviews()
            ],
          ),
        ),
      ),
    );
  }

  Container _addYourRating(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      height: 50,
      width: hBlock * 90,
      decoration: BoxDecoration(
        color: redColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          bottomRight: Radius.circular(20),
        ),
      ),
      child: FlatButton(
        child: MyText.sTextNL(text: 'Add Rate'),
        onPressed: () => _showRatingPopUp(context),
      ),
    );
  }

  _showRatingPopUp(
    BuildContext context,
  ) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return SimpleDialog(
          contentPadding: MediaQuery.of(context).viewInsets +
              const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(
                  width: 30,
                ),
                Text(
                  'Rate your experience',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
                IconButton(
                  iconSize: 30,
                  icon: Icon(Icons.close),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
            _ratingStars(),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                controller: _ratingController,
                textInputAction: TextInputAction.newline,
                keyboardType: TextInputType.multiline,
                maxLines: 4,
                decoration: InputDecoration(
                  labelText: 'Write your comment',
                  hintText: 'Write your comment',
                  hintStyle: TextStyle(
                    fontSize: hBlock * 5,
                  ),
                  labelStyle: TextStyle(
                    fontSize: hBlock * 5,
                  ),
                ),
              ),
            ),
            Container(
                margin: EdgeInsets.all(8),
                width: hBlock * 85,
                height: 40,
                child: GeneralBtn(
                  title: 'Add your rating',
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  width: hBlock * 30,
                )),
          ],
        );
      },
    );
  }

  Widget _ratingStars() {
    return StatefulBuilder(builder: (BuildContext context, setState) {
      return Center(
        child: RatingBar(
          initialRating: 0, // provider.getStars.toDouble(),,
          itemSize: hBlock * 13,
          itemCount: 5,
          itemBuilder: (context, _) => Icon(
            Icons.star,
            color: primaryColor,
          ),
          unratedColor: Colors.grey,
          textDirection: TextDirection.rtl,
          allowHalfRating: true,
          onRatingUpdate: (value) {
            setState(() {
              _stars = value.toInt();
            });
            print(_stars);
          },
        ),
      );
    });
  }

  Widget _topWidget(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          height: hBlock * 80,
          width: hBlock * 100,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/guitarist.png"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Positioned.directional(
            bottom: 10,
            start: hBlock * 10,
            textDirection:
                isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
            child: _nameRating()),
        Positioned.directional(
          textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
          child: IconButton(
            color: primaryColorLight,
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: Icon(
                Platform.isAndroid ? Icons.arrow_back : Icons.arrow_back_ios),
          ),
        )
      ],
    );
  }

  Widget _nameRating() {
    return Container(
      padding: EdgeInsets.all(8),
      width: hBlock * 80,
      decoration: BoxDecoration(
        color: Colors.black54.withOpacity(0.5),
        borderRadius: BorderRadius.all(
          Radius.circular(20),
        ),
      ),
      child: Center(
        child: Column(
          children: <Widget>[
            MyText.lTextNLBold(text: 'Amy Winehouse', color: redColor),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: MyText.sTextNL(
                  text: 'We Make you some cool songs and play  ',
                  color: canvusColor),
            ),
            MyRatingWidget.ratingStars(4)
          ],
        ),
      ),
    );
  }

  Widget _infoText({String title, String data}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 3.0),
      child: Row(
        children: <Widget>[
          MyText.sTextNLBold(text: 'Artist: '),
          MyText.sTextNL(text: 'Bob Marley (and the Wailers)'),
        ],
      ),
    );
  }

  Widget _appsWidget() {
    return Row(
      children: <Widget>[
        Container(
          child: SocialMediaButton.youtube(
            url: 'https:/www.youtube.com',
            size: 40,
            color: redColor,
            onTap: () {},
          ),
        ),
        SocialMediaButton.facebook(
            url: 'https:/www.facebook.com', size: 40, color: redColor),
        SocialMediaButton.spotify(
            url: 'https:/www.spotify.com', size: 40, color: redColor),
        SocialMediaButton.soundcloud(
            url: 'https:/www.soundcloud.com', size: 40, color: redColor),
        Container(),
      ],
    );
  }

  Widget _title(String title) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: MyText.xlTextNLBold(text: title, color: redColor),
    );
  }

  Widget _pricesList() {
    return SingleChildScrollView(
      child: ListView.builder(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemCount: 3,
          itemBuilder: (context, index) {
            return _pricesItem();
          }),
    );
  }

  Widget _pricesItem() {
    return Container(
      padding: EdgeInsets.all(hBlock * 1),
      margin: EdgeInsets.all(hBlock * 1),
      decoration: BoxDecoration(
        color: blackColor,
        borderRadius: BorderRadius.only(
          bottomRight: Radius.circular(20),
          topLeft: Radius.circular(20),
        ),
        border: Border.all(
            width: 1, color: primaryColorLight, style: BorderStyle.solid),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: MyText.sTextNL(text: '1H', color: canvusColor),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: MyText.sTextNL(text: '1000', color: canvusColor),
          ),
          RedCurvedBtn(
            title: 'Book now',
            createPage: () => BookingPage(),
            width: hBlock * 30,
          )
        ],
      ),
    );
  }

  Widget _customerQuestions() {
    return SingleChildScrollView(
      child: ListView.builder(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemCount: 5,
          itemBuilder: (context, index) {
            return _questionAnswer();
          }),
    );
  }

  Widget _questionAnswer() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 5.0),
          child: MyText.sTextNL(
              text: 'What if timing change After booking?', color: redColor),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 5.0),
          child: MyText.sTextNL(
              text:
                  'I will try my best to be flexible and adjust according to your needs. ',
              color: canvusColor),
        )
      ],
    );
  }
}
