import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/PreDefined/size_config.dart';
import 'package:popapp/Views/details_page.dart';
import 'package:popapp/Views/message_page.dart';
import 'package:popapp/Widgets/red_curved_btn_widget.dart';
import 'package:popapp/Widgets/custom_appbar_widget.dart';
import 'package:popapp/Widgets/rating_widget.dart';
import 'package:popapp/Widgets/text_widgets.dart';

class BookingsPage extends StatelessWidget {
  static const String tag = '/BookingsPage';
  @override
  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    //AppLocalizations  appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: SafeArea(
        bottom: false,
        child: Scaffold(
          backgroundColor: primaryColor,
          appBar: CustomAppBar.appBar(title: 'Bookings'),
          body: ListView(
            children: <Widget>[
              _title('Upcoming Events'),
              _subTitle('Tuesday, August 17  |  10:30 PM'),
              _categoryList(),
              _title('Past Events'),
              _subTitle('Tuesday, August 17  |  10:30 PM'),
              _categoryList(),
            ],
          ),
        ),
      ),
    );
  }

  ListView _categoryList() {
    return ListView.builder(
        shrinkWrap: true,
        physics: ScrollPhysics(),
        itemCount: 3,
        itemBuilder: (context, index) {
          return item(context);
        });
  }

  Widget _title(String title) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10),
      child: MyText.xlTextNLBold(text: title, color: redColor),
    );
  }

  Widget _subTitle(String title) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: MyText.lTextNL(text: title, color: primaryColorLight),
    );
  }

  Widget item(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DetailsPage(),
          ),
        );
      },
      child: Container(
        decoration: BoxDecoration(
          color: blackColor,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(25),
            bottomRight: Radius.circular(25),
          ),
          border:
              Border.all(width: 1, color: redColor, style: BorderStyle.solid),
        ),
        margin: EdgeInsets.all(hBlock * 1),
        // height: hBlock * 35,
        child: Row(
          children: <Widget>[
            _photoItem(),
            Container(
              width: hBlock * 53,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5),
                    child: MyText.lTextNLBold(
                        text: 'Amy Winehouse', color: canvusColor),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5),
                    child: Row(
                      children: <Widget>[
                        MyRatingWidget.ratingStars(4),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 10),
                          width: 1,
                          height: 15,
                          color: redColor,
                        ),
                        MyText.sTextNL(text: '20 Reviews', color: redColor),
                      ],
                    ),
                  ),
                  MyText.sTextNL(text: 'Event: Wedding', color: canvusColor),
                  MyText.sTextNL(text: 'Duration: 3H', color: canvusColor),
                  MyText.sTextNL(
                      text:
                          'Location: Cairo 5th settlement, block 201 after saoudi mall. ',
                      color: canvusColor),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 8),
                    child: Center(
                        child: RedCurvedBtn(
                      title: 'Send Message',
                      createPage: () => MessagePage(),
                      width: hBlock * 50,
                    )),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _photoItem() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25),
          bottomRight: Radius.circular(25),
        ),
        border: Border.all(width: 1, color: redColor, style: BorderStyle.solid),
      ),
      margin: EdgeInsets.all(hBlock * 2),
      width: hBlock * 40,
      height: hBlock * 40,
      child: ClipRRect(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(25),
            bottomRight: Radius.circular(25),
          ),
          child: Image.asset('assets/electric-guitar.png', fit: BoxFit.cover)),
    );
  }
}
