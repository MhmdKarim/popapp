import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';
import 'package:intl/intl.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/Widgets/options_widget.dart';
import 'package:popapp/Widgets/textFieldForm_widget.dart';
import 'package:popapp/Widgets/text_widgets.dart';

import '../Widgets/custom_appbar_widget.dart';

// List<GlobalKey<FormState>> formKeys = [
//   GlobalKey<FormState>(),
//   GlobalKey<FormState>(),
//   GlobalKey<FormState>(),
//   GlobalKey<FormState>()
// ];

//TODO: Temporary
class MyData {
  String name = '';
  String phone = '';
  String email = '';
  String age = '';
}

//TODO: temporary
class BoxSelection {
  bool isSelected;
  String title;
  BoxSelection({
    this.title,
    this.isSelected,
  });
}

class BookingPage extends StatefulWidget {
  static const tag = '/BookingPage';
  @override
  _BookingPageState createState() => _BookingPageState();
}

class _BookingPageState extends State<BookingPage> {
  int currStep = 0;
  static var _focusNode = FocusNode();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  static MyData data = MyData();
  static DateTime _selectedDate = DateTime.now();
  static BoxSelection _selectedDuration;
  static BoxSelection _selectedEvent;

  static var formattedSelectedDate = '';

  final _cityController = TextEditingController();
  final _areaController = TextEditingController();
  final _messageController = TextEditingController();

  DateTime _selectedTime;
  List<BoxSelection> _durationOption = List();
  List<BoxSelection> _eventOptions = List();
  //BoxSelection _selectedBox;

  DateTime _currentDate = DateTime.now();
  DateTime _currentDate2 = DateTime.now();
  String _currentMonth = DateFormat.yMMMM().format(DateTime.now());
  DateTime _targetDateTime = DateTime(2020, 2, 3);

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(() {
      setState(() {});
      print('Has focus: $_focusNode.hasFocus');
    });
    _durationOption.add(BoxSelection(title: "1 Hour", isSelected: false));
    _durationOption.add(BoxSelection(title: "2 Hours", isSelected: false));
    _durationOption.add(BoxSelection(title: "3 Hours", isSelected: false));

    _eventOptions.add(BoxSelection(title: "Wedding", isSelected: false));
    _eventOptions.add(BoxSelection(title: "Private Event", isSelected: false));
    _eventOptions
        .add(BoxSelection(title: "Corporate Event", isSelected: false));
  }

  _selectionDuration(data) {
    _selectedDuration = data;
    print(_selectedDuration.title);
  }

  _selectionEvent(data) {
    _selectedEvent = data;
    print(_selectedEvent.title);
  }

  Widget _timePicker() {
    return new TimePickerSpinner(
      is24HourMode: false,
      normalTextStyle: TextStyle(fontSize: sFont, color: canvusColor),
      highlightedTextStyle: TextStyle(
          fontSize: mFont, fontWeight: FontWeight.bold, color: redColor),
      spacing: 50,
      itemHeight: 80,
      isForce2Digits: true,
      onTimeChange: (time) {
        setState(() {
          _selectedTime = time;
        });
      },
    );
  }

  @override
  void dispose() {
    // _selectedDate = null;
    // _selectedTime = null;
    // _selectedDuration = null;
    // _selectedEvent = null;
    super.dispose();
  }

  void _showSnackBarMessage(String message, [MaterialColor color = redColor]) {
    Scaffold.of(context).showSnackBar(SnackBar(content: Text(message)));
  }

  void _submitDetails() {
    final FormState formState = _formKey.currentState;

    if (!formState.validate()) {
      _showSnackBarMessage('Please enter correct data');
    } else {
      formState.save();
      print("Name: ${data.name}");
      print("Phone: ${data.phone}");
      print("Email: ${data.email}");
      print("Age: ${data.age}");

      showDialog(
        context: context,
        child: AlertDialog(
          title: Text("Details"),
          //content:  Text("Hello World"),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text("Date : 01/01/2021"),
                Text("Time : 07:00 PM"),
                Text("Event : Wedding"),
                Text("City : Cairo"),
                Text("Area : Mohandesin"),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    List<Step> steps = [
      Step(
        title: Container(),
        isActive: true,
        state: StepState.indexed,
        content: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            MyText.lTextNL(text: 'When Is the event?', color: redColor),
            _calendarHeader(),
            _calendarCarousel()
            // Padding(
            //   padding: const EdgeInsets.symmetric(vertical: 100),
            //   child: ListTile(
            //     leading: Icon(
            //       Icons.calendar_today,
            //       color: canvusColor,
            //     ),
            //     title: Text(
            //       formattedSelectedDate == ''
            //           ? 'Select Date'
            //           : formattedSelectedDate,
            //       style: TextStyle(color: canvusColor),
            //     ),
            //     onTap: () {
            //       _selectDate(context);
            //     },
            //     trailing: Icon(
            //       Icons.arrow_drop_down,
            //       color: canvusColor,
            //     ),
            //   ),
            // ),
          ],
        ),
      ),
      Step(
          title: Container(),
          isActive: true,
          state: StepState.indexed,
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              MyText.lTextNL(
                  text: 'What Time would you like the musician to start?',
                  color: redColor),
              _redSeparator(),
              _timePicker(),
            ],
          )),
      Step(
        title: Container(),
        isActive: true,
        state: StepState.indexed,
        content: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            MyText.lTextNL(
                text: 'How long should they play for?', color: redColor),
            _redSeparator(),
            Options(
              option: _durationOption,
              selection: _selectionDuration,
            )
          ],
        ),
      ),
      Step(
          title: Container(),
          // subtitle: const Text('Subtitle'),
          isActive: true,
          state: StepState.indexed,
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              MyText.lTextNL(text: 'What is The event?', color: redColor),
              _redSeparator(),
              Options(
                option: _eventOptions,
                selection: _selectionEvent,
              )
            ],
          )),
      Step(
        title: Container(),
        isActive: true,
        state: StepState.indexed,
        content: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                MyText.lTextNL(text: 'Where is the event?', color: redColor),
                _redSeparator(),
                MyTextFields.textField(
                    hint: 'City',
                    controler: _cityController,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter City';
                      } else {
                        return null;
                      }
                    }),
                MyTextFields.textField(
                    hint: 'Area',
                    controler: _areaController,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter Area';
                      } else {
                        return null;
                      }
                    }),
                MyTextFields.textField(
                    hint: 'More Info',
                    maxLines: 5,
                    controler: _messageController,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter More Info';
                      } else {
                        return null;
                      }
                    }),
              ],
            ),
          ),
        ),
      )
    ];

    return SafeArea(
      bottom: false,
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: primaryColor,
        appBar: CustomAppBar.appBar(title: 'Booking'),
        body: Builder(
          builder: (context) => ListView(
            children: <Widget>[
              Container(
                width: double.infinity,
                height: vBlock * 120,
                child: Theme(
                  data: ThemeData(
                    canvasColor: primaryColor,
                    primaryColor: redColor,
                  ),
                  child: Stepper(
                    steps: steps,
                    type: StepperType.horizontal,
                    currentStep: this.currStep,
                    controlsBuilder: (BuildContext context,
                        {VoidCallback onStepContinue,
                        VoidCallback onStepCancel}) {
                      return Column(
                        // mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          _continueButton(onStepContinue),
                          Padding(
                            padding: EdgeInsets.all(10),
                          ),
                          // _backButton(onStepCancel),
                          // SizedBox(height: 100,)
                        ],
                      );
                    },
                    onStepContinue: () {
                      print(currStep);
                      // if (_formKey.currentState.validate()) {
                      //   if (currStep < steps.length - 1) {
                      //     currStep = currStep + 1;
                      //   } else {
                      //     currStep = 0;
                      //   }
                      // } else {
                      if (currStep == 0) {
                        if (_selectedDate == null) {
                          Scaffold.of(context).showSnackBar(
                              SnackBar(content: Text('please enter date')));
                        } else {
                          setState(() {
                            currStep = 1;
                          });
                        }
                      }
                      if (currStep == 1) {
                        if (_selectedTime == null) {
                          Scaffold.of(context).showSnackBar(
                              SnackBar(content: Text('please enter time')));
                        } else {
                          setState(() {
                            currStep = 2;
                          });
                        }
                      }
                      if (currStep == 2) {
                        if (_selectedDuration.title == null) {
                          Scaffold.of(context).showSnackBar(
                              SnackBar(content: Text('please enter duration')));
                        } else {
                          setState(() {
                            currStep = 3;
                          });
                        }
                      }
                      if (currStep == 3) {
                        if (_selectedEvent == null) {
                          Scaffold.of(context).showSnackBar(
                              SnackBar(content: Text('please enter event')));
                        } else {
                          setState(() {
                            currStep = 4;
                          });
                        }
                      }
                      if (currStep == 4) {
                        if (_formKey.currentState.validate()) {
                        } else {}
                      }
                    },
                    // onStepCancel: () {
                    //   setState(() {
                    //     if (currStep > 0) {
                    //       currStep = currStep - 1;
                    //     } else {
                    //       currStep = 0;
                    //     }
                    //   });
                    // },
                    // onStepTapped: (step) {
                    //   setState(() {
                    //     currStep = step;
                    //   });
                    // },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _redSeparator() {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 20),
        height: 3,
        width: 50,
        color: redColor);
  }

  Widget _continueButton(VoidCallback onStepContinue) {
    return Visibility(
      //visible: currStep != 4,
      child: Container(
        margin: EdgeInsets.only(top: 20),
        width: hBlock * 85,
        height: 50,
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25),
          ),
          onPressed: currStep != 4
              ? onStepContinue
              : () {
                  _submitDetails();
                },
          child: Text(currStep == 4 ? 'Sign up and finish' : 'Continue',
              style: TextStyle(color: Colors.white)),
          color: redColor,
        ),
      ),
    );
  }

  Widget _calendarHeader() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.arrow_left,
            color: canvusColor,
          ),
          onPressed: () {
            setState(() {
              _targetDateTime =
                  DateTime(_targetDateTime.year, _targetDateTime.month - 1);
              _currentMonth = DateFormat.yMMM().format(_targetDateTime);
            });
          },
        ),
        Text(
          _currentMonth,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 24.0,
            color: canvusColor,
          ),
        ),
        IconButton(
          icon: Icon(
            Icons.arrow_right,
            color: canvusColor,
          ),
          onPressed: () {
            setState(() {
              _targetDateTime =
                  DateTime(_targetDateTime.year, _targetDateTime.month + 1);
              _currentMonth = DateFormat.yMMM().format(_targetDateTime);
            });
          },
        )
      ],
    );
  }

  Widget _calendarCarousel() {
    return CalendarCarousel<Event>(
      daysTextStyle: TextStyle(color: canvusColor),
      selectedDayButtonColor: redColor,
      height: 250,
      childAspectRatio: 1.2,
      onDayPressed: (DateTime date, List<Event> events) {
        this.setState(() => _currentDate2 = date);
        print(_currentDate2);
        events.forEach((event) => print(event.title));
      },
      showOnlyCurrentMonthDate: true,
      thisMonthDayBorderColor: Colors.grey,
      weekFormat: false,
      selectedDateTime: _currentDate2,
      targetDateTime: _targetDateTime,
      showWeekDays: false,
      customGridViewPhysics: NeverScrollableScrollPhysics(),
      showHeader: false,
      todayTextStyle: TextStyle(
        color: Colors.blue,
      ),
      todayButtonColor: redColor,
      selectedDayTextStyle: TextStyle(
        color: Colors.white,
      ),
      weekendTextStyle: TextStyle(
        color: canvusColor,
      ),
      minSelectedDate: _currentDate.subtract(Duration(days: 360)),
      maxSelectedDate: _currentDate.add(Duration(days: 360)),
      inactiveDaysTextStyle: TextStyle(
        color: Colors.tealAccent,
        fontSize: 16,
      ),
      onCalendarChanged: (DateTime date) {
        this.setState(() {
          _targetDateTime = date;
          _currentMonth = DateFormat.yMMM().format(_targetDateTime);
        });
      },
      onDayLongPressed: (DateTime date) {
        print('long pressed date $date');
      },
    );
  }

  // Widget _backButton(VoidCallback onStepCancel) {
  //   return Visibility(
  //     visible: currStep != 0,
  //     child: Column(
  //       children: <Widget>[
  //         Container(
  //           width: hBlock * 85,
  //           height: 50,
  //           child: OutlineButton(
  //             shape: RoundedRectangleBorder(
  //               borderRadius: BorderRadius.circular(25),
  //             ),
  //             borderSide: BorderSide(
  //                 color: redColor, width: 1, style: BorderStyle.solid),
  //             onPressed: onStepCancel,
  //             child: const Text(
  //               'Back',
  //               style: TextStyle(color: Colors.white),
  //             ),
  //             color: redColor,
  //           ),
  //         ),
  //         SizedBox(height: 50)
  //       ],
  //     ),
  //   );
  // }

}
