import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/PreDefined/size_config.dart';
import 'package:popapp/Views/booking_page.dart';
import 'package:popapp/Views/details_page.dart';
import 'package:popapp/Widgets/red_curved_btn_widget.dart';
import 'package:popapp/Widgets/youtube_popup_widget.dart';
import 'package:popapp/Widgets/custom_appbar_widget.dart';
import 'package:popapp/Widgets/rating_widget.dart';
import 'package:popapp/Widgets/text_widgets.dart';

class SavedPage extends StatelessWidget {
  static const String tag = '/SavedPage';

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    //AppLocalizations  appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: SafeArea(
        bottom: false,
        child: Scaffold(
          backgroundColor: primaryColor,
          appBar: CustomAppBar.appBar(title: 'Saved'),
          body: ListView(
            children: <Widget>[
              _title('Singer'),
              _categoryList(),
              _title('Music'),
              _categoryList(),
            ],
          ),
        ),
      ),
    );
  }

  ListView _categoryList() {
    return ListView.builder(
        shrinkWrap: true,
        physics: ScrollPhysics(),
        itemCount: 3,
        itemBuilder: (context, index) {
          return item(context);
        });
  }

  Widget _title(String title) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: Center(child: MyText.xlTextNLBold(text: title, color: redColor)),
    );
  }

  Widget item(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DetailsPage(),
          ),
        );
      },
      child: Container(
        decoration: BoxDecoration(
          color: blackColor,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(25),
            bottomRight: Radius.circular(25),
          ),
          border:
              Border.all(width: 1, color: redColor, style: BorderStyle.solid),
        ),
        margin: EdgeInsets.all(hBlock * 1),
        // height: hBlock * 35,
        child: Row(
          children: <Widget>[
            _photoItem(),
            Container(
              width: hBlock * 62,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  MyText.lTextNLBold(text: 'Amy Winehouse', color: canvusColor),
                  Row(
                    children: <Widget>[
                      MyRatingWidget.ratingStars(4),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 10),
                        width: 1,
                        height: 15,
                        color: redColor,
                      ),
                      MyText.sTextNL(text: '20 Reviews', color: redColor),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: MyText.sTextNL(
                        text:
                            'Amy can sing pretty well and it will make your wedding something memorable. ',
                        color: canvusColor),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Container(
                          height: 30,
                          width: hBlock * 30,
                          decoration: BoxDecoration(
                            border: Border.all(
                                width: 1,
                                color: redColor,
                                style: BorderStyle.solid),
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              bottomRight: Radius.circular(20),
                            ),
                          ),
                          child: FlatButton(
                            onPressed: () {
                              MyPopup.showYouTubePopUp(context: context);
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.play_circle_outline,
                                  color: canvusColor,
                                ),
                                MyText.sTextNL(
                                    text: 'Watch', color: canvusColor),
                              ],
                            ),
                          ),
                        ),
                        RedCurvedBtn(
                          title: 'Book now',
                          createPage: () => BookingPage(),
                          width: hBlock * 30,
                        )
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _photoItem() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25),
          bottomRight: Radius.circular(25),
        ),
        border: Border.all(width: 1, color: redColor, style: BorderStyle.solid),
      ),
      margin: EdgeInsets.all(hBlock * 2),
      width: hBlock * 30,
      height: hBlock * 30,
      child: ClipRRect(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(25),
            bottomRight: Radius.circular(25),
          ),
          child: Image.asset('assets/electric-guitar.png', fit: BoxFit.cover)),
    );
  }
}
