import 'dart:io';

import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/Widgets/text_widgets.dart';

class RegisterUserPage extends StatelessWidget {
  static const tag = '/RegisterUserPage';
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: SafeArea(
        bottom: false,
        child: Scaffold(
          backgroundColor: primaryColor,
          body: Stack(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  image: DecorationImage(
                      image: AssetImage("assets/electric-guitar.png"),
                      fit: BoxFit.fitHeight),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  gradient: LinearGradient(
                      begin: FractionalOffset.topCenter,
                      end: FractionalOffset.bottomCenter,
                      colors: [
                        primaryColorLight.withOpacity(0.0),
                        redColor,
                      ],
                      stops: [
                        0.0,
                        1.0
                      ]),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        _backButton(context),
                        _logo(),
                      ],
                    ),
                    Spacer(),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: MyText.xlTextNL(text: 'Continue using'),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        _socialMediaBtn(
                            path: 'facebook.png',
                            onPressed: () {
                              //Navigator.of(context).pop();
                            }),
                        _socialMediaBtn(path: 'google+.jpg', onPressed: () {}),
                      ],
                    ),
                    Spacer(),
                    Container()
                    //  MyText.xlTextNL(text: '- or -'),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _socialMediaBtn({String path, Function onPressed}) {
    return ClipOval(
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image:AssetImage('assets/$path')
          )
        ),
        height: 80,
        width: 80,
        child: RawMaterialButton(
          onPressed: onPressed,
        ),
      ),
    );
  }

  Widget _backButton(BuildContext context) {
    return IconButton(
      color: primaryColorLight,
      onPressed: () {
        Navigator.of(context).pop();
      },
      icon: Icon(Platform.isAndroid ? Icons.arrow_back : Icons.arrow_back_ios),
    );
  }

  Widget _logo() {
    return Container(
        margin: EdgeInsets.all(20),
        height: hBlock * 20,
        width: hBlock * 20,
        child: Image.asset("assets/logo.png"));
  }
}
