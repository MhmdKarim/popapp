import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/Widgets/custom_appbar_widget.dart';
import 'package:popapp/Widgets/photo_widget.dart';
import 'package:popapp/Widgets/text_widgets.dart';

class VendorDetailsPage extends StatelessWidget {
  static const tag = '/VendorDetailPage';

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      child: Scaffold(
        backgroundColor: primaryColor,
        appBar: CustomAppBar.appBar(title: "Details"),
        body: ListView(
          children: [
            Container(
              child: PhotoWidget(
                ignoring: true,
                height: hBlock * 95,
                width: hBlock * 95,
              ),
            ),
            _detailRow(title: 'Instrument', details: 'Guitar'),
            _detailRow(title: 'Made in', details: 'Italy'),
            _detailRow(title: 'Price', details: 'starts from 1000 LE'),
            _detailRow(
                title: 'Company name',
                details: 'Horus for Musical Instruments'),
            _detailRow(
                title: 'Address',
                details: '11 Talaat harb st., DownTown, Cairo'),
            _detailRow(title: 'Telephone', details: '012345678890'),
          ],
        ),
      ),
    );
  }

  Widget _detailRow({String title, String details}) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          MyText.mTextNLBold(text: '$title : ', color: redColor),
          MyText.sTextNL(text: details, color: canvusColor),
        ],
      ),
    );
  }
}
