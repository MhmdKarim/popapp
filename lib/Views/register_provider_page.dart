import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/Widgets/textFieldForm_widget.dart';
import 'package:popapp/Widgets/text_widgets.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

class RegisterProviderPage extends StatefulWidget {
  static const String tag = '/RegisterProviderPage';

  @override
  _RegisterproviderPageState createState() => _RegisterproviderPageState();
}

class _RegisterproviderPageState extends State<RegisterProviderPage> {
  final RoundedLoadingButtonController _btnController =
      RoundedLoadingButtonController();
  final _formKey = GlobalKey<FormState>();

  final _phoneController = TextEditingController();
  final _emailController = TextEditingController();
  final _nameController = TextEditingController();
  final _fromController = TextEditingController();
  final _toController = TextEditingController();
  final _vidLink1Controller = TextEditingController();
  final _vidLink2Controller = TextEditingController();
  final _bioController = TextEditingController();

  String _selectedOption;
  List<String> _options = [
    'DJ/Mixer',
    'Singer',
    'Tapes',
    'Solo',
    'Music',
    'Sound'
  ];

  String _selectedLocation;
  List<String> _locations = ['Alex', 'Cairo', 'Tanta', 'Zagazig'];
  String _selectedGenre;
  List<String> _genre = ['Pop', 'Reggie', 'Rock n roll', 'Oriental'];
  String _selectedEvent;
  List<String> _events = ['Wedding', 'Party', 'Tohoor', 'Soboo3'];
  final _picker = ImagePicker();
  String _photoBase64;
  File _photoImage;
  @override
  Widget build(BuildContext context) {
    //SizeConfig().init(context);

    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: SafeArea(
        bottom: false,
        child: Scaffold(
          backgroundColor: primaryColor,
          body: Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: FractionalOffset.topCenter,
                  end: FractionalOffset.bottomCenter,
                  colors: [
                    blackColor,
                    primaryColorLight,
                  ],
                  stops: [
                    0.0,
                    1.0
                  ]),
              // image: DecorationImage(
              //     image: AssetImage("assets/guitarist.png"), fit: BoxFit.cover),
            ),
            child: ListView(
              children: <Widget>[
                _dropDownWidget(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    _leftSide(),
                    _form(),
                    _rightSide(),
                  ],
                ),
                _sendButton()
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: _formKey,
      child: Container(
        decoration: BoxDecoration(
          color: canvusColor,
          borderRadius: BorderRadius.all(
            Radius.circular(25),
          ),
        ),
        width: hBlock * 80,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(height: 10),
              MyTextFields.regTextField(
                  controler: _nameController,
                  hint: 'Name',
                  validator: (value) {
                    if (value.isEmpty) {
                      _btnController.reset();
                      return 'Please enter name';
                    } else {
                      return null;
                    }
                  }),
              MyTextFields.regTextField(
                  controler: _phoneController,
                  hint: 'Phone',
                  validator: (value) {
                    if (value.isEmpty) {
                      _btnController.reset();
                      return 'Please enter phone';
                    } else {
                      return null;
                    }
                  }),
              MyTextFields.regTextField(
                  controler: _emailController,
                  hint: 'Email',
                  validator: (value) {
                    if (value.isEmpty) {
                      _btnController.reset();
                      return 'Please enter email';
                    } else {
                      return null;
                    }
                  }),
              MyTextFields.regTextField(
                  controler: _vidLink1Controller,
                  hint: 'Video Link 1',
                  validator: (value) {
                    if (value.isEmpty) {
                      _btnController.reset();
                      return 'Please enter video link 1';
                    } else {
                      return null;
                    }
                  }),
              MyTextFields.regTextField(
                  controler: _vidLink2Controller,
                  hint: 'Video Link 2',
                  validator: (value) {
                    if (value.isEmpty) {
                      _btnController.reset();
                      return 'Please enter video link 2';
                    } else {
                      return null;
                    }
                  }),
              MyTextFields.regTextField(
                  controler: _fromController,
                  hint: 'Price from',
                  validator: (value) {
                    if (value.isEmpty) {
                      _btnController.reset();
                      return 'Please enter price from';
                    } else {
                      return null;
                    }
                  }),
              MyTextFields.regTextField(
                  controler: _toController,
                  hint: 'Price to',
                  validator: (value) {
                    if (value.isEmpty) {
                      _btnController.reset();
                      return 'Please enter price to';
                    } else {
                      return null;
                    }
                  }),
              _locationWidget(),
              _genreWidget(),
              _eventWidget(),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    _photoWidget(),
                    MyText.sTextNL(
                        text: 'Upload Your Photo', color: primaryColorLight)
                  ],
                ),
              ),
              Row(
                children: <Widget>[],
              ),
              MyTextFields.regTextField(
                  maxLines: 5,
                  hint: 'Please Write your bio',
                  controler: _bioController),
              SizedBox(height: 20)
            ],
          ),
        ),
      ),
    );
  }

  Widget _locationWidget() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: DropdownButtonFormField<String>(
        value: _selectedLocation,
        items: _locations.map((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          );
        }).toList(),
        hint: Text('Location'),
        onChanged: (value) {
          setState(() {
            _selectedLocation = value;
          });
        },
      ),
    );
  }

  Widget _dropDownWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        IconButton(
          color: primaryColorLight,
          onPressed: () {
            Navigator.of(context).pop();
          },
          iconSize: 30,
          icon: Icon(
              Platform.isAndroid ? Icons.arrow_back : Icons.arrow_back_ios),
        ),
        Container(
          width: hBlock * 70,
          height: 50,
          decoration: BoxDecoration(
              color: blackColor,
              border: Border.all(
                color: redColor,
              ),
              borderRadius: BorderRadius.all(Radius.circular(20))),
          margin: EdgeInsets.symmetric(vertical: 20),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: DropdownButtonFormField<String>(
              isDense: false,
              itemHeight: 50,
              iconEnabledColor: canvusColor,
              style: TextStyle(color: canvusColor),
              value: _selectedOption,
              items: _options.map((value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: MyText.sTextNL(text: value, color: redColor),
                );
              }).toList(),
              hint: MyText.mTextNL(
                  text: 'Select your service', color: canvusColor),
              onChanged: (value) {
                setState(() {
                  _selectedOption = value;
                });
              },
            ),
          ),
        ),
        IconButton(
          color: Colors.transparent,
          onPressed: () {},
          iconSize: 30,
          icon: Icon(Icons.add),
        ),
      ],
    );
  }

  Widget _genreWidget() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: DropdownButtonFormField<String>(
        value: _selectedGenre,
        items: _genre.map((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          );
        }).toList(),
        hint: Text('Genre'),
        onChanged: (value) {
          setState(() {
            _selectedGenre = value;
          });
        },
      ),
    );
  }

  Widget _eventWidget() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: DropdownButtonFormField<String>(
        value: _selectedEvent,
        items: _events.map((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          );
        }).toList(),
        hint: Text('Event'),
        onChanged: (value) {
          setState(() {
            _selectedEvent = value;
          });
        },
      ),
    );
  }

  Widget _rightSide() {
    return Container(
      decoration: BoxDecoration(
        color: redColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(40),
          bottomLeft: Radius.circular(40),
        ),
      ),
      width: hBlock * 5,
      height: vBlock * 50,
    );
  }

  Widget _leftSide() {
    return Container(
      decoration: BoxDecoration(
        color: redColor,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(40),
          bottomRight: Radius.circular(40),
        ),
      ),
      width: hBlock * 5,
      height: vBlock * 50,
    );
  }

  Widget _sendButton() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: RoundedLoadingButton(
        height: 60,
        width: hBlock * 80,
        child: MyText.mTextNL(text: "Submit", color: canvusColor),
        controller: _btnController,
        color: redColor,
        onPressed: () {
          Navigator.of(context).pop();
          //if (_formKey.currentState.validate()) {}
        },
      ),
    );
  }

  Widget _photoWidget() {
    return Container(
      //padding: const EdgeInsets.only(top: 10, bottom: 5),
      child: InkWell(
        onTap: () {
          _pickImage(ImageSource.gallery);
        },
        child: Center(
          child: Stack(
            overflow: Overflow.visible,
            fit: StackFit.passthrough,
            children: <Widget>[
              ClipRRect(
                  child: _photoImage == null
                      ? CircleAvatar(
                          radius: (hBlock * 25 / 2),
                          backgroundColor: redColor,
                          child: CircleAvatar(
                            backgroundColor: canvusColor,
                            radius: hBlock * 12,
                          ),
                        )
                      : Container(
                          decoration: BoxDecoration(
                              color: redColor, shape: BoxShape.circle),
                          child: CircleAvatar(
                            backgroundImage: FileImage(_photoImage),
                            backgroundColor: Colors.transparent,
                            // maxRadius: 48,
                            minRadius: hBlock * 22,
                          ),
                          height: hBlock * 25,
                          width: hBlock * 25,
                        )),
              Positioned(
                top: -hBlock * 20,
                bottom: 0,
                right: -hBlock * 2.75,
                child: IconButton(
                  onPressed: () {
                    _pickImage(ImageSource.gallery);
                  },
                  icon: Icon(
                    Icons.add_circle,
                    size: hBlock * 8,
                    color: redColor,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _pickImage(ImageSource _imageSource) async {
    PickedFile pickedFile = await _picker.getImage(source: _imageSource);
    final File _file = File(pickedFile.path);
    List<int> imageBytes = _file.readAsBytesSync();
    _photoBase64 = base64Encode(imageBytes);
    if (_file != null) {
      setState(() {
        _photoImage = _file;
      });
    }
  }
}
