import 'package:flutter/material.dart';
import 'package:popapp/Generics/validator.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/PreDefined/size_config.dart';
import 'package:popapp/Widgets/custom_appbar_widget.dart';
import 'package:popapp/Widgets/textFieldForm_widget.dart';
import 'package:popapp/Widgets/text_widgets.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

class ContactUsPage extends StatefulWidget {
  static const tag = '/ContactUsPage';
  @override
  _ContactUsPageState createState() => _ContactUsPageState();
}

class _ContactUsPageState extends State<ContactUsPage> {
  final _emailController = TextEditingController();
  final _subjectController = TextEditingController();
  final _messageController = TextEditingController();
  final RoundedLoadingButtonController _btnController =
      RoundedLoadingButtonController();
  String email;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);

    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: SafeArea(
        bottom: false,
        child: Scaffold(
          backgroundColor: primaryColor,
          resizeToAvoidBottomInset: true,
          appBar: CustomAppBar.appBar(title: "Contact us"),
          body: Container(
            padding: EdgeInsets.all(hBlock * 2),
            color: primaryColor,
            child: Form(
           key:   _formKey,
                          child: Center(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      MyText.xlTextNLBold(
                          text: 'Your Email Address', color: redColor),
                      MyTextFields.textField(
                        controler: _emailController,
                        hint: 'Your Email Address',
                        
                        validator: (value) {
                          if (value.isEmpty) {
                            _btnController.reset();
                            return "Please enter your email";
                          } else {
                            return Validator.validateEmail(context, value);
                          }
                        },
                        type: TextInputType.text,
                        obscureText: false,
                      ),
                      MyText.xlTextNLBold(text: 'Subject', color: redColor),
                      MyTextFields.textField(
                        controler: _subjectController,
                        hint: 'Subject',
                        // onChanged: (value) {
                        //   email = value;
                        // },
                        validator: (value) {
                          if (value.isEmpty) {
                            _btnController.reset();
                            return "Please enter your subject";
                          } else {
                            return Validator.validateEmail(context, value);
                          }
                        },
                        type: TextInputType.text,
                        obscureText: false,
                      ),
                      MyText.xlTextNLBold(text: 'Your message', color: redColor),
                      MyTextFields.textField(
                        maxLines: 5,
                        controler: _messageController,
                        hint: 'Your message',
                        // onChanged: (value) {
                        //   email = value;
                        // },
                        validator: (value) {
                          if (value.isEmpty) {
                            _btnController.reset();
                            return "Please enter your message";
                          } else {
                            return Validator.validateEmail(context, value);
                          }
                        },
                        type: TextInputType.text,
                        obscureText: false,
                      ),
                      
                      _sendButton(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _sendButton() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: RoundedLoadingButton(
        height: 60,
        width: hBlock * 85,
        child: MyText.mTextNL(text: "Submit", color: canvusColor),
        controller: _btnController,
        color: redColor,
        onPressed: () {

             if (_formKey.currentState.validate()) {}
        },
      ),
    );
  }
}
