import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/PreDefined/localization.dart';
import 'package:popapp/PreDefined/size_config.dart';
import 'package:popapp/Views/category_page.dart';
import 'package:popapp/Views/products.dart';
import 'package:popapp/Widgets/custom_appbar_widget.dart';
import 'package:popapp/Widgets/on_will_pop_widget.dart';
import 'package:popapp/Widgets/photo_widget.dart';

import 'package:popapp/Widgets/red_curved_btn_widget.dart';
import 'package:popapp/Widgets/side_menu_widget.dart';
import 'package:popapp/Widgets/text_widgets.dart';

class HomePage extends StatefulWidget {
  static const tag = '/HomePage';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  AppLocalizations appLocalizations;

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return WillPopScope(
      onWillPop: () {
        return ExitingApp.onWillPop(context);
      },
      child: Directionality(
        textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
        child: SafeArea(
          bottom: false,
          child: Scaffold(
            backgroundColor: primaryColor,
            appBar: CustomAppBar.appBar(title: "Home"),
            drawer: SideMenu(),
            body: Padding(
              padding: EdgeInsets.all(hBlock * 2),
              child: ListView(
                children: <Widget>[
                  MyText.xlTextNLBold(text: 'Market Place', color: redColor),
                  Container(
                    height: hBlock * 50,
                    child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: 6,
                        itemBuilder: (context, index) {
                          return PhotoWidget(
                            height: hBlock * 35,
                            width: hBlock * 35,
                          );
                        }),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 30),
                    child: RedCurvedBtn(
                      title: 'Buy an instrument',
                      width: hBlock * 60,
                      height: 50,
                      createPage: () => ProductsPage(),
                    ),
                  ),
                  // Container(
                  //   height: hBlock * 35,
                  //   child: ListView.builder(
                  //       shrinkWrap: true,
                  //       scrollDirection: Axis.horizontal,
                  //       itemCount: 10,
                  //       itemBuilder: (context, index) {
                  //         return PhotoWidget();
                  //       }),
                  // ),
                  MyText.xlTextNLBold(text: 'Book a service', color: redColor),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      _serviceItemLeft(
                          title: 'Singer', image: 'guitar', icon: 'Microphone'),
                      _serviceItemRight(
                          title: 'Music Band', image: 'guitar', icon: 'wave'),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      _serviceItemLeft(
                          title: 'Dj/Mixer', image: 'guitar', icon: 'DJ'),
                      _serviceItemRight(
                          title: 'Solo', image: 'guitar', icon: 'Piano'),
                    ],
                  ),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.spaceAround,
                  //   children: <Widget>[
                  //     _serviceItemLeft(
                  //         title: 'Tapes', image: 'guitar', icon: 'Cassette'),
                  //     _serviceItemRight(
                  //         title: 'Sound', image: 'guitar', icon: 'PlayButton'),
                  //   ],
                  // ),
                  // MyText.xlTextNLBold(text: 'Recent Reviews', color: redColor),
                  // RecentReviews()
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _serviceItemRight({String title, String image, String icon}) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CategoryPage(
              image: icon,
              title: title,
            ),
          ),
        );
      },
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(
                'assets/$image.png',
              ),
              fit: BoxFit.cover),
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(25),
            bottomLeft: Radius.circular(25),
          ),
          border:
              Border.all(width: 1, color: redColor, style: BorderStyle.solid),
        ),
        margin: EdgeInsets.all(hBlock * 2),
        width: hBlock * 42,
        height: hBlock * 42,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(hBlock * 2),
                height: hBlock * 15,
                width: hBlock * 15,
                child: Image.asset('assets/$icon.png'),
              ),
              MyText.lTextNL(text: title, color: canvusColor),
            ],
          ),
        ),
      ),
    );
  }

  Widget _serviceItemLeft({String title, String image, String icon}) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CategoryPage(
              image: icon,
              title: title,
            ),
          ),
        );
      },
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(
                'assets/$image.png',
              ),
              fit: BoxFit.cover),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(25),
            bottomRight: Radius.circular(25),
          ),
          border:
              Border.all(width: 1, color: redColor, style: BorderStyle.solid),
        ),
        margin: EdgeInsets.all(hBlock * 2),
        width: hBlock * 42,
        height: hBlock * 42,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(hBlock * 2),
              height: hBlock * 15,
              width: hBlock * 15,
              child: Image.asset('assets/$icon.png'),
            ),
            MyText.lTextNL(text: title, color: canvusColor),
          ],
        ),
      ),
    );
  }
}
