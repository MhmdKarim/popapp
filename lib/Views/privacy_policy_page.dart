import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/Widgets/custom_appbar_widget.dart';
import 'package:popapp/Widgets/text_widgets.dart';

class PrivacyPolicyPage extends StatelessWidget {
static const String tag = '/PrivacyPolicyPage';

  @override
  Widget build(BuildContext context) {
    //SizeConfig().init(context);

    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: SafeArea(
         bottom: false,
              child: Scaffold(
                backgroundColor: primaryColor,
          appBar: CustomAppBar.appBar(title:"Privacy Policy"),
          body: Container(
            
            child: Center(child:  MyText.lTextNL(text:"Privacy Policy")),
            //   child: FutureBuilder<Abouts>(
            //     future: about(),
            //     builder: (context, snapshot) {
            //       if (snapshot.hasData) {
            //         return Padding(
            //           padding: const EdgeInsets.all(14.0),
            //           child: SingleChildScrollView(
            //             child: Directionality(
            //               textDirection: TextDirection.rtl,
            //               child: Column(
            //                 children: <Widget>[
            //                   Text(
            //                     snapshot.data.title,
            //                     style: TextStyle(color: Colors.white),
            //                   ),
            //                   Text(
            //                     snapshot.data.description,
            //                     textAlign: TextAlign.justify,
            //                     style: TextStyle(
            //                       color: Colors.white,
            //                     ),
            //                   ),
            //                 ],
            //               ),
            //             ),
            //           ),
            //         );
            //       } else if (snapshot.hasError) {
            //         return Text("${snapshot.error}");
            //       }
            //       return AlertDialog(
            //         backgroundColor: Colors.transparent,
            //         shape: RoundedRectangleBorder(
            //             borderRadius: BorderRadius.circular(5)),
            //         elevation: 10,
            //         content: Container(
            //           height: 150,
            //           width: 150,
            //           child: Center(
            //             child: Column(
            //               mainAxisAlignment: MainAxisAlignment.center,
            //               crossAxisAlignment: CrossAxisAlignment.center,
            //               children: <Widget>[
            //                 SpinKitCircle(color: Theme.of(context).primaryColor),
            //                 Text(
            //                   'برجاء الانتطار ...',
            //                   style: TextStyle(color: Theme.of(context).primaryColor),
            //                 )
            //               ],
            //             ),
            //           ),
            //         ),
            //       );
            //     },
            //   ),
          ),
        ),
      ),
    );
  }
}
