import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:popapp/PreDefined/app_constants.dart';

class MyRatingWidget {
  static Widget ratingStars(double rate) {
    return IgnorePointer(
      child: RatingBar(
        initialRating: rate, // provider.getStars.toDouble(),,
        itemSize: hBlock * 5,
        itemCount: 5,
        itemBuilder: (context, _) => Icon(
          Icons.star,
          color: redColor,
        ),
        unratedColor: Colors.grey,
        textDirection: TextDirection.rtl,
        allowHalfRating: true,
        onRatingUpdate: (value) {},
      ),
    );
  }
}
