import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/Views/contact_us_page.dart';
import 'package:popapp/Views/privacy_policy_page.dart';
import 'package:popapp/Views/register_provider_page.dart';
import 'package:popapp/Views/register_user.dart';
import 'package:popapp/Views/bookings_page.dart';
import 'package:popapp/Views/saved_page.dart';
import 'package:popapp/Views/terms_conditions_page.dart';
import 'package:popapp/Widgets/text_widgets.dart';
import 'package:popapp/Widgets/user_photo.dart';

class SideMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: ClipRRect(
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(25), bottomRight: Radius.circular(25)),
        child: Container(
          width: hBlock * 70,
          decoration: BoxDecoration(
            color: primaryColor,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(25),
              bottomRight: Radius.circular(25),
            ),
            border:
                Border.all(width: 2, color: redColor, style: BorderStyle.solid),
          ),
          child: Column(
            children: <Widget>[
              _photoWidget(),
              Container(
                height: 10,
                width: hBlock * 80,
                color: redColor,
              ),
              _drawerItem(
                  title: "Service provider Sign up",
                  image: 'wave',
                  context: context,
                  path: RegisterProviderPage.tag),
              _drawerItem(
                  title: "User Sign up",
                  image: 'person',
                  context: context,
                  path: RegisterUserPage.tag),
              _drawerItem(
                title: "Saved",
                image: 'Heart',
                context: context,
                path: SavedPage.tag,
              ),
              _drawerItem(
                  title: "Bookings",
                  image: 'reservation',
                  context: context,
                  path: BookingsPage.tag),
              _drawerItem(
                  title: "Terms Of Services",
                  image: 'Terms',
                  context: context,
                  path: TermsConditionsPage.tag),
              _drawerItem(
                  title: "Privacy Policy",
                  image: 'privacy',
                  context: context,
                  path: PrivacyPolicyPage.tag),
              _drawerItem(
                  title: "Contact Us",
                  image: 'contact',
                  context: context,
                  path: ContactUsPage.tag),
              Spacer(),
              _drawerItemExit(
                  title: "Log out",
                  image: 'exit',
                  context: context,
                  function: () {}),
              SizedBox(
                height: 25,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _photoWidget() {
    return Stack(
      children: <Widget>[
        Container(
          height: vBlock * 30,
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(25),
              //  bottomRight: Radius.circular(25),
            ),
            image: DecorationImage(
                image: AssetImage("assets/electric-guitar.png"),
                fit: BoxFit.cover),
          ),
        ),
        Container(
          alignment: Alignment.center,
          height: vBlock * 30,
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
            gradient: LinearGradient(
                begin: FractionalOffset.topCenter,
                end: FractionalOffset.bottomCenter,
                colors: [
                  primaryColorLight.withOpacity(0.0),
                  redColor.withOpacity(0.7),
                ],
                stops: [
                  0.0,
                  1.0
                ]),
          ),
          child: Center(child: UserPhoto(height: 100, imageUrl: null)),
        ),
      ],
    );
  }

  Widget _drawerItem({String title, String image, context, String path}) {
    return Material(
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 10),
        color: primaryColor,
        child: InkWell(
          onTap: () {
            Navigator.of(context).pop();
            Navigator.of(context).pushNamed(path);
            print(path);
          },
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Container(
                  height: 25,
                  width: 25,
                  child: Image.asset(
                    'assets/$image.png',
                    fit: BoxFit.contain,
                  ),
                ),
              ),
              MyText.mTextNL(text: title, color: canvusColor),
            ],
          ),
        ),
      ),
    );
  }

  Widget _drawerItemExit(
      {String title, String image, context, Function function}) {
    return Material(
      child: Container(
        color: primaryColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            InkWell(
              onTap: function,
              child: Padding(
                padding: const EdgeInsets.only(left: 8, right: 8, bottom: 8),
                child: Row(
                  children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.only(left: 10, right: 10),
                        child: Container(
                            height: 25,
                            width: 25,
                            child: Image.asset(
                              'assets/$image.png',
                              fit: BoxFit.contain,
                            ))),
                    MyText.mTextNL(text: title, color: canvusColor),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
