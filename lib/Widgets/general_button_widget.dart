import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/Widgets/text_widgets.dart';

class GeneralBtn extends StatelessWidget {
  final String title;
  final double width;
  final double height;

  final Function onPressed;

  const GeneralBtn({this.title, this.onPressed, this.width, this.height});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        color: redColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          bottomRight: Radius.circular(20),
        ),
      ),
      child: FlatButton(
        onPressed: onPressed,
        child: Center(
          child: MyText.sTextNL(text: title, color: canvusColor),
        ),
      ),
    );
  }
}
