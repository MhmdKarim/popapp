import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/app_constants.dart';

class UserPhoto extends StatelessWidget {
  final String imageUrl;
  final double height;
  const UserPhoto({this.imageUrl, this.height});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: CircleAvatar(
        radius: (height / 2) + 2,
        backgroundColor: primaryColor,
        child: imageUrl == null
            ? ClipOval(
                //radius: size -2,
                child: Image.asset(
                  'assets/person.png',
                  fit: BoxFit.cover,
                  height: height,
                  width: height,
                ),
              )
            : ClipOval(
                child: CachedNetworkImage(
                  height: height,
                  width: height,
                  imageUrl: imageUrl,
                  fit: BoxFit.cover,
                  placeholder: (context, url) =>
                      Image.asset('assets/person.png'),
                  errorWidget: (context, url, error) =>
                      Image.asset('assets/person.png'),
                ),
              ),
      ),
    );
  }
}
