import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/PreDefined/size_config.dart';
import 'package:popapp/Widgets/text_widgets.dart';

class NoDataAvailable extends StatelessWidget {
  final String image;
  final String text;
  final bool visible;
  final Function onPressed;
  const NoDataAvailable({
    this.image,
    this.text,
    this.onPressed,
    this.visible,
  });

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Container(
        height: (vBlock * 100) - 100,
        width: hBlock * 100,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              image,
              height: hBlock * 50,
              width: hBlock * 50,
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: MyText.mText(context: context, text: text, ),
            ),
            Visibility(
              visible: visible,
              child: IconButton(
                  icon: Icon(Icons.refresh),
                  color: primaryColor,
                  iconSize: hBlock * 20,
                  onPressed: onPressed),
            )
          ],
        ),
      ),
    );
  }
}
