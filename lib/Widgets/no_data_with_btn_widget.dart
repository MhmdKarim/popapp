import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/PreDefined/size_config.dart';
import 'package:popapp/Widgets/standard_button_widget.dart';
import 'package:popapp/Widgets/text_widgets.dart';


class NoDataAvailableWithBtn extends StatelessWidget {
  final String image;
  final String text;
  const NoDataAvailableWithBtn({
    this.image,
    this.text,
  });

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Container(
        height: (vBlock * 100) -100,
        width: hBlock * 100,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              image,
              height: hBlock * 50,
              width: hBlock * 50,
            ),
            SizedBox(
              height: 20,
            ),
            MyText.mText(context: context, text: text,),
           Btn.button(
             context:context,
             title: 'Add_now',
             onPressed: (){
              // Navigator.of(context).pushNamed(AddRejectRequestReasons.tag);
             }
           )
          ],
        ),
      ),
    );
  }
}
