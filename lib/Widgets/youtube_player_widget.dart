import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/cupertino.dart';


class YouTubeWidget extends StatefulWidget {
  @override
  _YouTubeWidgetState createState() => _YouTubeWidgetState();
}

class _YouTubeWidgetState extends State<YouTubeWidget> {
  YoutubePlayerController _controller;
  bool _isPlayerReady = false;
  PlayerState _playerState;
  YoutubeMetaData _videoMetaData;
  @override
  void initState() {
    super.initState();
    _controller = YoutubePlayerController(
      initialVideoId: 'gQDByCdjUXw',
      flags: YoutubePlayerFlags(
        mute: false,
        //autoPlay: false,
        disableDragSeek: false,
        loop: false,
        isLive: false,
        forceHD: false,
        enableCaption: false,
      ),
    );
    //..addListener(listener);
    _videoMetaData = YoutubeMetaData();
    _playerState = PlayerState.unknown;
  }

  void listener() {
    if (_isPlayerReady && mounted && !_controller.value.isFullScreen) {
      setState(() {
        _playerState = _controller.value.playerState;
        _videoMetaData = _controller.metadata;
      });
    }
  }

  @override
  void deactivate() {
    // Pauses video while navigating to next page.
    _controller.pause();
    super.deactivate();
  }

  // @override
  // void dispose() {
  //   if (mounted) {
  //     _controller.dispose();
  //     // _idController.dispose();
  //     // _seekToController.dispose();
  //   }
  // }
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      padding: EdgeInsets.all(3.0),
      child: YoutubePlayer(
       // width: hBlock * 90,
        
        controller: _controller,
        showVideoProgressIndicator: true,
        progressIndicatorColor: redColor,
        onReady: () {
          _isPlayerReady = true;
        },
      ),
    );
  }
}
