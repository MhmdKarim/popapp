import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/Widgets/text_widgets.dart';

class ExitingApp {
  static Future<bool> onWillPop(BuildContext context) async {
    return (await showDialog(
          context: context,
          builder: (context) => new SimpleDialog(
            backgroundColor: primaryColorLight,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 20, 20, 30),
                child: Center(
                  child: MyText.lTextNL(
                      text: 'Sure, You wanna Exit?', color: redColor),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: hBlock * 30,
                    height: vBlock * 6,
                    margin: EdgeInsets.only(top: 20, bottom: 20),
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(10)),
                    child: RawMaterialButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(vBlock),
                      ),
                      onPressed: () {
                        SystemNavigator.pop();
                      },
                      fillColor: redColor,
                      child: MyText.lTextNL(text: 'yes', color: canvusColor),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    width: hBlock * 30,
                    height: vBlock * 6,
                    margin: EdgeInsets.only(top: 20, bottom: 20),
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(10)),
                    child: RawMaterialButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(vBlock),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop(false);
                      },
                      fillColor: Colors.green,
                      child: MyText.lTextNL(text: 'cancel', color: canvusColor),
                    ),
                  )
                ],
              ),
            ],
          ),
        )) ??
        false;
  }
}
