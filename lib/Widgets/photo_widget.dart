import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/Views/vendor_detail_page.dart';
import 'package:popapp/Widgets/text_widgets.dart';

class PhotoWidget extends StatelessWidget {
  final double height;
  final double width;
  final bool visible;
  final bool ignoring;

  const PhotoWidget(
      {this.height, this.width, this.visible = true, this.ignoring = false});

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: ignoring,
      child: InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => VendorDetailsPage(),
            ),
          );
        },
        child: Column(
          children: [
            Stack(
              overflow: Overflow.visible,
              fit: StackFit.passthrough,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      bottomLeft: Radius.circular(25),
                    ),
                    border: Border.all(
                        width: 1, color: redColor, style: BorderStyle.solid),
                  ),
                  margin: EdgeInsets.all(hBlock * 2),
                  width: width,
                  height: height,
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      bottomLeft: Radius.circular(25),
                    ),
                    child: Image.asset('assets/guitar.png', fit: BoxFit.cover),
                  ),
                ),
                Positioned(
                  right: hBlock * 2,
                  bottom: 20,
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.all(5),
                        color: redColor.withOpacity(0.7),
                        child: Center(
                          child: MyText.xsTextNL(text: 'From 500 LE'),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Visibility(
              visible: visible,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: MyText.sTextNLBold(text: 'Alexa Shop'),
              ),
            )
          ],
        ),
      ),
    );
  }
}
