import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/app_constants.dart';

class MyTextFields {
  static Widget textField(
      {TextEditingController controler,
      int maxLines = 1,
      String hint,
      Function onChanged,
      String initialValue,
      Function validator,
      TextInputType type,
      bool obscureText = false}) {
    return Container(
      width: hBlock * 85,
      padding: EdgeInsets.symmetric(vertical: 10),
      child: TextFormField(
        maxLines: maxLines,
        style: TextStyle(fontSize: hBlock * 4, color: canvusColor),
        controller: controler,
        obscureText: obscureText,
        autofocus: true,
        initialValue: initialValue,
        keyboardType: type,
        cursorColor: canvusColor,
        decoration: InputDecoration(
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(25),
            ),
            borderSide: BorderSide(
              color: redColor,
              width: 2.0,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(25),
            ),
            borderSide: BorderSide(
              color: redColor,
              width: 2.0,
            ),
          ),
          alignLabelWithHint: true,
         
          fillColor: canvusColor,
          hoverColor: canvusColor,
          focusColor: canvusColor,
          //labelText: hint,
          hintText: hint,
          hintStyle: TextStyle(fontSize: hBlock * 4, color: canvusColor),
          labelStyle: TextStyle(fontSize: hBlock * 4, color: canvusColor),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          counterStyle: TextStyle(color: canvusColor),
        ),
        onChanged: onChanged,
        validator: validator,
      ),
    );
  }

  static Widget regTextField(
      {TextEditingController controler,
      int maxLines = 1,
      String hint,
      Function onChanged,
      String initialValue,
      Function validator,
      TextInputType type,
      bool obscureText = false}) {
    return Container(
     // width: hBlock * 85,
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: TextFormField(
        maxLines: maxLines,
        style: TextStyle(fontSize: hBlock * 4, color: primaryColor),
        controller: controler,
        obscureText: obscureText,
        autofocus: true,
        initialValue: initialValue,
        keyboardType: type,
        cursorColor: redColor,
        decoration: InputDecoration(
          alignLabelWithHint: true,
          fillColor: canvusColor,
          hoverColor: canvusColor,
          focusColor: canvusColor,
          //labelText: hint,
          hintText: hint,
          hintStyle: TextStyle(fontSize: hBlock * 4, color: primaryColorLight),
          //labelStyle: TextStyle(fontSize: hBlock * 4, color: primaryColorLight),
        ),
        onChanged: onChanged,
        validator: validator,
      ),
    );
  }


}
