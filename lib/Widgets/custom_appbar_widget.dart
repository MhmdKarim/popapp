import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'text_widgets.dart';

class CustomAppBar {
  static Widget appBarWithLogo({String title, String image}) {
    return PreferredSize(
      preferredSize: Size(double.infinity, 80),
      child: AppBar(
        iconTheme: IconThemeData(color: primaryColorLight),
        shape: ContinuousRectangleBorder(
          side: BorderSide(width: 3, color: redColor),
          borderRadius: new BorderRadius.vertical(
              bottom: new Radius.elliptical(double.infinity, 100.0)),
          // borderRadius: BorderRadius.only(
          //   bottomLeft: Radius.circular(40),
          //   bottomRight: Radius.circular(40),
          // ),
        ),
        elevation: 0,
        backgroundColor: primaryColor,
        flexibleSpace: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 50,
              // width: 100,
              child: Image.asset(
                'assets/$image.png',
                fit: BoxFit.contain,
              ),
            ),
            SizedBox(width: 15),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                MyText.xlTextNLBold(text: title, color: canvusColor),
                Container(
                    transform: Matrix4.translationValues(-10.0, 0.0, 0.0),
                    height: 10,
                    width: hBlock * 6,
                    child: Image.asset(
                      'assets/Shape.png',
                      fit: BoxFit.fitWidth,
                    ))
              ],
            ),
          ],
        ),
        centerTitle: true,
      ),
    );
  }

  static Widget appBar({String title, bool visible = false}) {
    return PreferredSize(
      preferredSize: Size.fromHeight(80),
      child: AppBar(
        iconTheme: IconThemeData(color: primaryColorLight),
        shape: ContinuousRectangleBorder(
          side: BorderSide(width: 3, color: redColor),
          borderRadius: BorderRadius.vertical(
              bottom: Radius.elliptical(double.infinity, 80.0)),
        ),
        elevation: 0,
        backgroundColor: primaryColor,
        actions: <Widget>[],
        flexibleSpace: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              MyText.xlTextNLBold(text: title, color: canvusColor),
              Container(
                transform: Matrix4.translationValues(-10.0, 0.0, 0.0),
                height: 10,
                width: hBlock * 6,
                child: Image.asset(
                  'assets/Shape.png',
                  fit: BoxFit.fitWidth,
                ),
              )
            ],
          ),
        ),
        centerTitle: true,
      ),
    );
  }
}
