import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/Widgets/youtube_player_widget.dart';

class MyPopup {
  static showYouTubePopUp({BuildContext context}) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return Directionality(
          textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
                      child: SimpleDialog(
                        backgroundColor: primaryColor,
              children: <Widget>[
                YouTubeWidget(),
              ],
            ),
          ),
        );
      },
    );
  }

// static   Future<bool> onWillPop(BuildContext context,String message) async {
//     return MyPopup.showPopUp(
//       context: context,
//       title: AppLocalizations.of(context).getMessageByLangAndKey(
//           appLanguageCode, 'confirmation'),
//       message: AppLocalizations.of(context).getMessageByLangAndKey(
//           appLanguageCode, message),
//       function: () {
//         SystemNavigator.pop();
//       },
//     )??
//     false;
//   }

}
