import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/Widgets/rating_widget.dart';
import 'package:popapp/Widgets/text_widgets.dart';
import 'package:popapp/Widgets/user_photo.dart';

class RecentReviews extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: ListView.builder(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemCount: 5,
          itemBuilder: (context, index) {
            return _recentReviews();
          }),
    );
  }

  Widget _recentReviews() {
    return Container(
      decoration: BoxDecoration(
        color: blackColor,
        borderRadius: BorderRadius.only(
          bottomRight: Radius.circular(25),
        ),
        border: Border.all(width: 1, color: redColor, style: BorderStyle.solid),
      ),
      margin: EdgeInsets.all(hBlock * 1),
      height: hBlock * 35,
      child: Row(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                    top: hBlock * 1, right: hBlock * 1, left: hBlock * 1),
                width: hBlock * 25,
                height: hBlock * 25,
                decoration: BoxDecoration(
                  border: Border.all(
                      width: 1, color: redColor, style: BorderStyle.solid),
                ),
                child: UserPhoto(height: 80, imageUrl: null),
              ),
              MyRatingWidget.ratingStars(4)
            ],
          ),
          Container(
            width: hBlock * 64,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                MyText.sTextNL(text: 'MARWA hired BOBBY', color: redColor),
                MyText.sTextNL(text: 'For a Wedding', color: redColor),
                Container(height: 2, width: hBlock * 15, color: redColor),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: MyText.sTextNL(
                      text:
                          'He\'s a Really good singer he\'s on point and he can\'t really amazing ',
                      color: canvusColor),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
