import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/Views/booking_page.dart';
import 'package:popapp/Widgets/text_widgets.dart';

class Options extends StatefulWidget {
  final List<BoxSelection> option;
  final Function(BoxSelection selected) selection;

  const Options({this.option, this.selection, });

  @override
  _OptionsState createState() => _OptionsState();
}

class _OptionsState extends State<Options> {
  

  BoxSelection _selectedBox;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: widget.option.length,
      shrinkWrap: true,
      physics: ScrollPhysics(),
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () {
            setState(() {
              if (_selectedBox != null) {
                _selectedBox.isSelected = false;
              }
              widget.option[index].isSelected =
                  !widget.option[index].isSelected;
              _selectedBox = widget.option[index];
              widget.selection(_selectedBox);
              //print(_selectedBox.title);
            });
          },
          child: Container(
            // width: hBlock * 50,
            margin: EdgeInsets.all(5),
            padding: EdgeInsets.all(5),
            height: 50,
            decoration: BoxDecoration(
              color: widget.option[index].isSelected ? redColor : blackColor,
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(25),
              ),
              border: Border.all(
                color: redColor,
                width: 2.0,
              ),
            ),
            child: MyText.mTextNL(text: widget.option[index].title),
          ),
        );
      },
    );
  }
}
