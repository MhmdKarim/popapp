import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/app_constants.dart';
import 'package:popapp/Widgets/text_widgets.dart';

class RedCurvedBtn extends StatelessWidget {
  final String title;
  final double width;
  final double height;

  final Function() createPage;

  const RedCurvedBtn(
      {this.title, this.createPage, this.width, this.height = 30});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        color: redColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          bottomRight: Radius.circular(20),
        ),
      ),
      child: FlatButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => createPage(),
            ),
          );
        },
        child: Center(
          child: MyText.sTextNL(text: title, color: canvusColor),
        ),
      ),
    );
  }
}
