import 'package:flutter/material.dart';
import 'package:popapp/Widgets/text_widgets.dart';
import '../PreDefined/app_constants.dart';

class Btn {
  static Widget button(
      {BuildContext context, String title, Function onPressed}) {
    return Container(
      margin: EdgeInsets.all(20),
      width: hBlock * 85,
      height: 50,
      child: RaisedButton(
          color: primaryColor,
          elevation: 2,
          onPressed: onPressed,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: MyText.mText(
            context: context,
            text: title,
            color: Colors.white,
          )),
    );
  }
}
