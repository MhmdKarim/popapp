import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/localization.dart';
import '../PreDefined/app_constants.dart';

class MyText {
  static Widget xsText({BuildContext context, String text, Color color = canvusColor}) {
    AppLocalizations appLocalizations = AppLocalizations.of(context);
    return Text(
      appLocalizations.getMessageByLangAndKey(appLanguageCode, text),
      style: TextStyle(color: color, fontSize: xsFont),
    );
  }

  static Widget xsTextNL({String text, Color color = canvusColor}) {
    return Text(
      text,
      style: TextStyle(color: color, fontSize: xsFont),
    );
  }

  static Widget sText({BuildContext context, String text, Color color = canvusColor}) {
    AppLocalizations appLocalizations = AppLocalizations.of(context);
    return Text(
      appLocalizations.getMessageByLangAndKey(appLanguageCode, text),
      style: TextStyle(color: color, fontSize: sFont),
    );
  }

  static Widget sTextBold({BuildContext context, String text, Color color = canvusColor}) {
    AppLocalizations appLocalizations = AppLocalizations.of(context);
    return Text(
      appLocalizations.getMessageByLangAndKey(appLanguageCode, text),
      style:
          TextStyle(color: color, fontSize: sFont, fontWeight: FontWeight.bold),
    );
  }

  static Widget sTextNL({String text, Color color = canvusColor}) {
    return Text(
      text,
      style: TextStyle(color: color, fontSize: sFont),
    );
  }

  static Widget sTextNLBold({ String text, Color color = canvusColor}) {
    return Text(
      text,
      style:
          TextStyle(color: color, fontSize: sFont, fontWeight: FontWeight.bold),
    );
  }

  static Widget mText({BuildContext context, String text, Color color = canvusColor}) {
    AppLocalizations appLocalizations = AppLocalizations.of(context);
    return Text(
      appLocalizations.getMessageByLangAndKey(appLanguageCode, text),
      style: TextStyle(color: color, fontSize: mFont),
    );
  }

  static Widget mTextBold({BuildContext context, String text, Color color = canvusColor}) {
    AppLocalizations appLocalizations = AppLocalizations.of(context);
    return Text(
      appLocalizations.getMessageByLangAndKey(appLanguageCode, text),
      style:
          TextStyle(color: color, fontSize: mFont, fontWeight: FontWeight.bold),
    );
  }

  static Widget mTextNL({String text, Color color = canvusColor}) {
    return Text(
      text,
      style: TextStyle(color: color, fontSize: mFont),
    );
  }

  static Widget mTextNLBold({String text, Color color = canvusColor}) {
    return Text(
      text,
      style:
          TextStyle(color: color, fontSize: mFont, fontWeight: FontWeight.bold),
    );
  }

  static Widget lText({BuildContext context, String text, Color color = canvusColor}) {
    AppLocalizations appLocalizations = AppLocalizations.of(context);
    return Text(
      appLocalizations.getMessageByLangAndKey(appLanguageCode, text),
      style: TextStyle(color: color, fontSize: lFont),
    );
  }

  static Widget lTextBold({BuildContext context, String text, Color color = canvusColor}) {
    AppLocalizations appLocalizations = AppLocalizations.of(context);
    return Text(
      appLocalizations.getMessageByLangAndKey(appLanguageCode, text),
      style:
          TextStyle(color: color, fontSize: lFont, fontWeight: FontWeight.bold),
    );
  }

  static Widget lTextNL({String text, Color color = canvusColor}) {
    return Text(
      text,
      style: TextStyle(color: color, fontSize: lFont),
    );
  }

  static Widget lTextNLBold({String text, Color color = canvusColor}) {
    return Text(
      text,
      style:
          TextStyle(color: color, fontSize: lFont, fontWeight: FontWeight.bold),
    );
  }

  static Widget xlText({BuildContext context, String text, Color color = canvusColor}) {
    AppLocalizations appLocalizations = AppLocalizations.of(context);
    return Text(
      appLocalizations.getMessageByLangAndKey(appLanguageCode, text),
      style: TextStyle(color: color, fontSize: xlFont),
    );
  }

  static Widget xlTextBold({BuildContext context, String text, Color color = canvusColor}) {
    AppLocalizations appLocalizations = AppLocalizations.of(context);
    return Text(
      appLocalizations.getMessageByLangAndKey(appLanguageCode, text),
      style: TextStyle(
          color: color, fontSize: xlFont, fontWeight: FontWeight.bold),
    );
  }

  static Widget xlTextNL({String text, Color color = canvusColor}) {
    return Text(
      text,
      style: TextStyle(color: color, fontSize: xlFont),
    );
  }

  static Widget xlTextNLBold({String text, Color color = canvusColor}) {
    return Text(
      text,
      style: TextStyle(
          color: color, fontSize: xlFont, fontWeight: FontWeight.bold),
    );
  }
}
