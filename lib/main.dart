import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'Views/products.dart';
import 'Views/register_provider_page.dart';
import 'Views/register_user.dart';
import 'Views/saved_page.dart';
import 'Views/terms_conditions_page.dart';
import 'PreDefined/localization.dart';
import 'PreDefined/app_constants.dart';
import 'Views/home_page.dart';
import 'Views/contact_us_page.dart';
import 'Views/privacy_policy_page.dart';
import 'Views/bookings_page.dart';
import 'Views/booking_page.dart';
import 'Views/vendor_detail_page.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  // SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(
      //DevicePreview(builder: (context) => MyApp(),enabled: !kReleaseMode,));
      MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Locale _locale;
  //Initial value before any selection is made

  final routes = <String, WidgetBuilder>{
    TermsConditionsPage.tag: (context) => TermsConditionsPage(),
    PrivacyPolicyPage.tag: (context) => PrivacyPolicyPage(),
    ContactUsPage.tag: (context) => ContactUsPage(),
    SavedPage.tag: (context) => SavedPage(),
    BookingPage.tag: (context) => BookingPage(),
    BookingsPage.tag: (context) => BookingsPage(),
    RegisterProviderPage.tag: (context) => RegisterProviderPage(),
    RegisterUserPage.tag: (context) => RegisterUserPage(),
    ProductsPage.tag: (context) => ProductsPage(),
    VendorDetailsPage.tag: (context) => VendorDetailsPage(),
  };

  @override
  void initState() {
    super.initState();
    _locale = appLanguageId == 1 ? Locale('en') : Locale('ar');
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pop',
      theme: ThemeData(
        primarySwatch: Colors.red,
        primaryColor: redColor,
        // accentColor: accentColor,
        //  fontFamily: 'Tajawal',
        cursorColor: redColor,
        textSelectionColor: primaryColor,
        textSelectionHandleColor: primaryColor,
      ),
      debugShowCheckedModeBanner: false,
      home: HomePage(),
      localizationsDelegates: [
        const AppLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', ''),
        const Locale('ar', ''),
      ],
      routes: routes,
      locale: _locale,
    );
  }
}
