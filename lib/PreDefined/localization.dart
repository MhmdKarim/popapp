﻿import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show SynchronousFuture;

class AppLocalizations {
  AppLocalizations(this.locale);

  final Locale locale;

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'ar': {
      'Select_App_Language': 'اختر لغة التطبيق',
      'Welcome_Again': 'أهلا بك مرة اخرى',
      'Welcome': 'مرحبا',
      'Categories': 'الأقسام',
      'Home': 'الرئيسية',
      'Requests': 'الطلبات',
      'More': 'المزيد',
      'Other_Branches': 'باقي الفروع',
      'Ratings': 'التقييمات',
      'Book_Your_Place_Now': 'أحجز مكانك الأن',
      'See_More': 'شاهد الكل',
      'Book_Now': 'أحجز الأن',
      'Branches': 'الفروع',
      'Successfully_Reserved': 'تم الحجز بنجاح',
      'Follow_Your_Request': 'تابع طلبك',
      'Dont_Have_Account?': 'ليس لديك حساب؟',
      'Please_Login': 'قم بتسجيل الدخول',
      'Login': 'تسجيل الدخول',
      'Please_Login_With_Phone': 'قم بتسجيل الدخول بواسطة رقم الهاتف',
      'Create_New_Account': 'إنشاء حساب جديد',
      'Sorry_Not_registered':
          'نأسف انت لست مسجل من قبل من فضلك قم بملئ هذه البيانات',
      'Name': 'الاسم',
      'Phone_Number': 'رقم الهاتف',
      '01xxxxxxxxxx': '01xxxxxxxxxx',
      'Password': 'كلمة المرور',
      'New_Account_Created': 'تم إنشاء حساب جديد',
      'Account_Created_Reserve_Easy':
          'حاليا تم انشاء الحساب يمكنك الحجز بسهولة',
      'Thank_You': 'شكرا',
      'Update_Profile': 'تعديل الملف الشخصي',
      'About_us': 'من نحن',
      'notifications': 'الإشعارات',
      'messages': 'الرسائل',
      'change_language': 'تغيير اللغة',
      'terms_conditions': 'الشروط و الاحكام',
      'privacy_policy': 'سياسة الخصوصية',
      'contact_us': 'تواصل معنا',
      'exit': 'تسجيل الخروج',
      'confirmation': 'تأكيد',
      'yes': 'نعم',
      'no': 'لا',
      'are_you_sure': 'هل انت متأكد ؟',
      'All': 'الكل',
      'Rate_your_Experience': 'قيم تجربتك',
      'Write_your_Rating': 'أكتب تقييمك',
      'Add_your_Rating': 'اضف تقييمك',
      'Sure_Cancel_reservation?': 'هل انت متأكد من إلغاء الحجز فى',
      'Edit_Profile': 'تعديل الملف الشخصي',
      'Write_your_Request': 'اكتب استفسارك',
      'Booking_Date': 'تاريخ الحجز',
      'Time': 'الوقت',
      'No_Persons': 'عدد الأفراد',
      'Request_Status': 'حالة الطلب',
      'Waiting_Confirmation': 'منتظر التأكيد',
      'Cancel_Booking': 'إلغاء الحجز',
      'Booking_Canceled': 'الحجز ملغي',
      'Thanks_for_Rating': 'شكرا لتقيمك',
      'Confirmed': 'مؤكد',
      'Filter': 'فلتر',
      'Delete_All': 'مسح الكل',
      'Done': 'تم',
      'Country': 'البلد',
      'City': 'المدينة',
      'District': 'المنطقة',
      'Category': 'القسم',
      'Subcategory': 'القسم الفرعي',
      'Search': 'البحث',
      'Search_No_Results': 'نأسف لا يوجد نتائج بحث حاول البحث مرة اخرى',
      'Add_Your_Rating': 'اضف تقييمك',
      'Favourites_are_empty': 'المفضلة فارغة',
      'Favourites': 'المفضلة',
      'Select_time': 'أختر الوقت',
      'Write_your_notes': 'أكتب ملاحظاتك',
      'Notes': 'الملاحظات',
      'Select': 'أختر',
      'Back_to_registration': 'الرجوع الى تسجيل الدخول',
      'Please_write_your_registered_email':
          'يرجي كتابة البريد الإلكتروني المسجل به',
      'New_password': 'كلمة المرور الجديد',
      'New_password_confirmation': 'تأكيد كلمة المرور الجديدة',
      'Change_password': 'تغيير كلمة المرور',
      'Logo': 'الشعار',
      'Bookings': 'الحجوزات',
      'New_booking': 'طلب حجز جديد',
      'New_message': 'رسالة جديدة',
      'You_got_a_new_message': 'لديك رسالة جديدة من',
      'Settings': 'الإعدادات',
      'Current_password': 'كلمة المرور الحالية',
      'out_of': 'من أصل',
      'stars': 'نجوم',
      'Rating': 'تقييم',
      'Password_and_Confirm': ' كلمة المرور و تاكيد كلمة المرور غير متطابقان',
      'Enter': 'ادخل',
      'are_you_sure_exit': 'هل أنت متأكد أنك تريد تسجيل الخروج',
      'are_you_sure_close_app': 'هل أنت متأكد أنك تريد إغلاق التطبيق',
      // '': '',
      // '': '',
      // '': '',
      // '': '',
      // '': '',
      // '': '',
      // '': '',
      // '': '',
      // '': '',
      // '': '',
    },
    'en': {
      'Select_App_Language': 'Select App Language',

      // '': '',
      // '': '',
      // '': '',
      // '': '',
      // '': '',
      // '': '',
    },
  };

  // String getMessageByKey(appLanguageCode,String key) {
  //   return _localizedValues[locale.languageCode][key];
  // }
  String getMessageByLangAndKey(String appLanguageCode, String key) {
    String val = _localizedValues[appLanguageCode][key];
    return val ?? 'No Value for this string: $key';
  }
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'ar'].contains(locale.languageCode);

  @override
  Future<AppLocalizations> load(Locale locale) {
    return SynchronousFuture<AppLocalizations>(AppLocalizations(locale));
  }

  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;
}
