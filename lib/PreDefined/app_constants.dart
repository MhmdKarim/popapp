import 'package:flutter/material.dart';
import 'package:popapp/PreDefined/size_config.dart';


int currentbarIndex = 0;
int appLanguageId = 1;
String appLanguageCode = 'en';
bool isRightToLeft = false;
//LoginModel constLoginModel;
double vBlock = SizeConfig.vBlock;
double hBlock = SizeConfig.hBlock;
double xsFont = SizeConfig.xsFont;

double sFont = SizeConfig.sFont;
double mFont = SizeConfig.mFont;
double lFont = SizeConfig.lFontSize;
double xlFont = SizeConfig.xlFont;
double screenWidth = SizeConfig.screenWidth;
double screenHeight = SizeConfig.screenHeight;

//const Color titleColor = const Color(0XFFa7bdca);
//const Color textColor = const Color(0XFF4d555b);
const MaterialColor primaryColor = MaterialColor(0XFF1B1B1B, <int, Color>{});
const MaterialColor primaryColorLight =
    MaterialColor(0XFF707070, <int, Color>{});
const MaterialColor redColor = MaterialColor(0XFFB32928, <int, Color>{});
const MaterialColor blackColor = MaterialColor(0XFF151515, <int, Color>{});

const MaterialColor canvusColor = MaterialColor(0XFFFFFFFF, <int, Color>{});

